package com.example.pichurchyk_p2.util

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment

fun ShowDialog(title: String, message: String, context: Context, permanent: Boolean) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setTitle(title)
    builder.setMessage(message)
    if (!permanent){
        builder.setPositiveButton("Admit", null)
    }
    else {
        builder.setCancelable(false)
    }
    val dialog: AlertDialog = builder.create()
    dialog.show()
}
