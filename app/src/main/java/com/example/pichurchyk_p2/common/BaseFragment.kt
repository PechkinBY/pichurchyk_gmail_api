package com.example.pichurchyk_p2.common

import android.content.Context
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.example.pichurchyk_p2.util.ShowDialog
import com.example.pichurchyk_p2.util.toast

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {

    fun toast(message: String) {
        requireContext().toast(message)
    }
    fun dialog(title: String, message: String, context: Context, permanent: Boolean){
        ShowDialog(title, message, context, permanent)
    }
}
