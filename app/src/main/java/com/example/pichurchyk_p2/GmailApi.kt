package com.example.pichurchyk_p2

//import android.content.Context
//import android.util.Log
//import com.google.api.client.auth.oauth2.Credential
//import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
//import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
//import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
//import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
//import com.google.api.client.http.javanet.NetHttpTransport
//import com.google.api.client.json.JsonFactory
//import com.google.api.client.json.jackson2.JacksonFactory
//import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
//import com.google.api.client.repackaged.org.apache.commons.codec.binary.StringUtils
//import com.google.api.client.util.Base64.decodeBase64
//import com.google.api.client.util.StringUtils
//import com.google.api.client.util.store.FileDataStoreFactory
//import com.google.api.services.gmail.Gmail
//import com.google.api.services.gmail.GmailScopes
//import com.google.api.services.gmail.model.Label
//import com.google.api.services.gmail.model.ListLabelsResponse
//import com.google.api.services.gmail.model.ListMessagesResponse
//import com.google.api.services.gmail.model.Message
//import java.io.File
//import java.io.FileNotFoundException
//import java.io.IOException
//import java.io.InputStreamReader
//import java.util.*


class GmailApi {
//    private val APPLICATION_NAME = "Gmail API"
//    private val JSON_FACTORY: JsonFactory = JacksonFactory.getDefaultInstance()
//    private val TOKENS_DIRECTORY_PATH = "tokens"
//
//    private val SCOPES: List<String> = Collections.singletonList(GmailScopes.GMAIL_READONLY)
//    private val CREDENTIALS_FILE_PATH = "/credentials.json"
//
//
//    @Throws(IOException::class)
//    private fun getCredentials(HTTP_TRANSPORT: NetHttpTransport, context: Context): Credential {
//        // Load client secrets.
//        val `in` = GmailApi::class.java.getResourceAsStream(CREDENTIALS_FILE_PATH)
//            ?: throw FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH)
//        val clientSecrets: GoogleClientSecrets =
//            GoogleClientSecrets.load(JSON_FACTORY, InputStreamReader(`in`))
//        // Build flow and trigger user authorization request.
//        val tokenFolder = getTokenFolder(context)
//        if (!tokenFolder.exists()) {
//            tokenFolder.mkdir()
//        }
//        val flow = GoogleAuthorizationCodeFlow.Builder(
//            HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES
//        )
//            .setDataStoreFactory(FileDataStoreFactory(getTokenFolder(context)))
//            .setAccessType("offline")
//            .build()
//
////        val receiver: LocalServerReceiver =
////            LocalServerReceiver.Builder()
////                .setPort(8888).build()
//
//        return AuthorizationCodeInstalledApp(flow, LocalServerReceiver()).authorize("user")
//    }
//
//    private fun getTokenFolder(context: Context): File {
//        return File(context.getExternalFilesDir("")?.absolutePath + TOKENS_DIRECTORY_PATH)
//    }
//
//    fun main(context: Context) {
//        val HTTP_TRANSPORT = com.google.api.client.http.javanet.NetHttpTransport()
//        val service: Gmail = com.google.api.services.gmail.Gmail.Builder(
//            HTTP_TRANSPORT,
//            JSON_FACTORY,
//            getCredentials(HTTP_TRANSPORT, context)
//        )
//            .setApplicationName(APPLICATION_NAME)
//            .build()
//
//        var allEmails: Map<String, String>? = null
//        val user = "me"
//        val listResponse: ListLabelsResponse = service.users().labels().list(user).execute()
//        val labels: List<Label> = listResponse.getLabels()
//        val request = service.users().messages().list(user)
//        val messagesResponse: ListMessagesResponse = request.execute()
//        request.pageToken = messagesResponse.nextPageToken
//        val messagesList: List<Message> = messagesResponse.messages
//        for (i in 0..messagesList.size) {
//            val messageId: String = messagesResponse.messages[i].id
//            val currMessage = service.users().messages().get(user, messageId).execute()
//            val currMessBody = StringUtils
//                .newStringUtf8(Base64.decodeBase64(currMessage.payload.parts[0].body.data))
//
//            val emailsMap = mutableListOf<Map<String, String>>()
//            val currEmail = mutableMapOf<String, String>()
//            for (value in currMessage.payload.headers) {
//                when (value.name) {
//                    "From" -> {
//                        currEmail["From"] = value.value
//                    }
//                    "Subject" -> {
//                        if (value.value.isEmpty()) {
//                            currEmail["Subject"] = "No subject"
//                        } else {
//                            currEmail["Subject"] = value.value
//                        }
//                    }
//                    "Date" -> {
//                        currEmail["Date"] = value.value
//                    }
//                    else -> {
//                        false
//                    }
//                }
//                emailsMap.add(currEmail)
//            }
//            println(emailsMap[1])
//            allEmails = emailsMap[1]
//        }
//        Log.d("lol", "lol")
//    }
}