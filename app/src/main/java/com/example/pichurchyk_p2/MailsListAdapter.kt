package com.example.pichurchyk_p2

import android.content.Context
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.api.client.util.Base64
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.model.MessagePart
import com.google.api.services.gmail.model.MessagePartBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class MailsListAdapter(
    private val mails: MutableList<Map<String, String>>,
    private val mailsAttachments: MutableList<MessagePart?>,
    private val service: Gmail?,
    private val context: Context
) :
    RecyclerView.Adapter<MailsListAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView? = null
        var subject: TextView? = null
        var sender: TextView? = null
        var attachments: LinearLayout? = null
        var wholeMessage: ConstraintLayout? = null

        init {
            date = itemView.findViewById(R.id.message_date)
            subject = itemView.findViewById(R.id.message_subject)
            sender = itemView.findViewById(R.id.message_sender)
            attachments = itemView.findViewById(R.id.message_attach)
            wholeMessage = itemView.findViewById(R.id.message)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.mail_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.date?.text = mails[position]["Date"]
        holder.subject?.text = mails[position]["Subject"]
        holder.sender?.text = mails[position]["From"]
        if (service != null) {
            if (mails[position]["Attachments"] != "false") {
                holder.attachments?.visibility = View.VISIBLE
                holder.wholeMessage?.setOnClickListener {
                    GlobalScope.async(Dispatchers.Main) {
                        if (mailsAttachments[holder.adapterPosition] == null) {
                            Toast.makeText(context, "Attachment with unexpected extension cannot be saved!", Toast.LENGTH_SHORT).show()
                        }
                        else {
                            downloadAttachments(
                                mailsAttachments[holder.adapterPosition]!!,
                                service
                            )
                        }
                    }
                }
            }
        }
    }

    override fun getItemCount() = mails.size

    suspend fun downloadAttachments(part: MessagePart, service: Gmail) {
        val filename: String = part.filename
        val attId: String = part.body.attachmentId
        var attachPart: MessagePartBody? = null
        val file = File(
            "${
                Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS
                )
            }/${filename}"
        )
        try {
            try {
                attachPart =
                    withContext(Dispatchers.IO) {
                        service.users().messages().attachments().get("me", part.partId, attId)
                            .execute()
                    }
            } catch (ex: Exception) {
                println("------------------------${ex.stackTraceToString()}")
            }
            val fileByteArray: ByteArray = Base64.decodeBase64(attachPart?.data)
            file.createNewFile()
            val fOut = FileOutputStream(file)
            fOut.write(fileByteArray)
            fOut.close()
            Toast.makeText(context, "Attachment was saved to Downloads", Toast.LENGTH_SHORT).show()
            println("------------------------READY")
        } catch (ex: IOException) {
            println("------------------------${ex.stackTraceToString()}")
        }
    }

}