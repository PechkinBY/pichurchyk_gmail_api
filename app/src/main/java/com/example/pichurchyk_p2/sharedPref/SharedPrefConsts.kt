package com.example.l2_t1_p.sharedPref

object SharedPrefConsts {
    const val USER_NAME = "KEY_USER_NAME"
    const val REMEMBER_USER = "KEY_USER_REMEMBER"
    const val USER_EMAIL = "KEY_USER_EMAIL"
}