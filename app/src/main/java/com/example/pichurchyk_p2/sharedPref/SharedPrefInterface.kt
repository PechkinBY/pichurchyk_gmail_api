package com.example.l2_t1_p.sharedPref

import android.content.Context
import android.content.Context.MODE_PRIVATE

class SharedPrefInterface(context: Context) {

    private val sharedPref = context.getSharedPreferences("userData", MODE_PRIVATE)

    fun setPref(key: String ,value: Any){
        val editor = sharedPref?.edit()
        when (value) {
            is Int -> editor?.putInt(key, value)
            is String -> editor?.putString(key, value)
            is Boolean -> editor?.putBoolean(key, value)
            is Float -> editor?.putFloat(key, value)
        }
        editor?.apply()
    }

    fun getPrefString(key: String) : Any? {
        return sharedPref?.getString(key, null)
    }
    fun getPrefInt(key: String) : Int? {
        return sharedPref?.getInt(key, 0)
    }
    fun getPrefBool(key: String) : Boolean? {
        return sharedPref?.getBoolean(key, false)
    }
    fun getPrefFloat(key: String) : Float? {
        return sharedPref?.getFloat(key, 0f)
    }
}