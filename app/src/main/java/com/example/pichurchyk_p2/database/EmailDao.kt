package com.example.pichurchyk_p2.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface EmailDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addEmail(email: Email)

    @Query("Select * FROM email_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<Email>>
}