package com.example.pichurchyk_p2.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "email_table")
data class Email(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val sender: String,
    val subject: String,
    val date: String
)