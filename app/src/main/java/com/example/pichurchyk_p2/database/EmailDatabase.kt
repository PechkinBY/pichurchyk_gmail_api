package com.example.pichurchyk_p2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Email::class], version = 1, exportSchema = false)
abstract class EmailDatabase : RoomDatabase() {

    abstract fun emailDao() : EmailDao

    companion object{
        @Volatile
        private var INSTANCE : EmailDatabase? = null

        fun getDatabase(context: Context) : EmailDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    EmailDatabase::class.java,
                    "email_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
