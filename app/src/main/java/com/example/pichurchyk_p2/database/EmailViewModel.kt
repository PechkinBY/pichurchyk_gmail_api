package com.example.pichurchyk_p2.database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EmailViewModel(application: Application) : AndroidViewModel(application) {
    val readAllData : LiveData<List<Email>>
    private val repository : EmailRepository

    init {
        val emailDao = EmailDatabase.getDatabase(application).emailDao()
        repository = EmailRepository(emailDao)
        readAllData = repository.readAllData
    }

    fun addEmail(email: Email){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addEmail(email)
        }
    }
}