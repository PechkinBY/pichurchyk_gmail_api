package com.example.pichurchyk_p2.database

import androidx.lifecycle.LiveData

class EmailRepository(private val emailDao: EmailDao) {

    val readAllData: LiveData<List<Email>> = emailDao.readAllData()

    suspend fun addEmail(email: Email){
        emailDao.addEmail(email)
    }

}