package com.example.pichurchyk_p2

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.pichurchyk_p2.fragments.MailsList
import com.google.android.gms.common.api.internal.LifecycleCallback.getFragment


@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        this.finish()
    }
}