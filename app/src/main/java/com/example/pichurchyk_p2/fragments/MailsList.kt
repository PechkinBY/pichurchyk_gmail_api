package com.example.pichurchyk_p2.fragments

import android.accounts.Account
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.l2_t1_p.sharedPref.SharedPrefConsts
import com.example.l2_t1_p.sharedPref.SharedPrefInterface
import com.example.pichurchyk_p2.BuildConfig
import com.example.pichurchyk_p2.MailsListAdapter
import com.example.pichurchyk_p2.R
import com.example.pichurchyk_p2.common.BaseFragment
import com.example.pichurchyk_p2.database.Email
import com.example.pichurchyk_p2.database.EmailViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.api.client.extensions.android.json.AndroidJsonFactory
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.GmailScopes
import com.google.api.services.gmail.model.ListMessagesResponse
import com.google.api.services.gmail.model.Message
import com.google.api.services.gmail.model.MessagePart
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*


class MailsList : BaseFragment(R.layout.fragment_mails_list) {

    var sharedPref: SharedPrefInterface? = null

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var btnSignOut: Button? = null

    private var service: Gmail? = null
    private lateinit var credential: GoogleAccountCredential
    private val RQ_GMAIL_ACCESS = 120

    private var mails: MutableList<Map<String, String>>? = null
    private var mailsAttachments: MutableList<MessagePart?> = mutableListOf()

    private lateinit var mView: View
    private lateinit var adapter: MailsListAdapter

    private lateinit var emailViewModel: EmailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mails = mutableListOf()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
        sharedPref = SharedPrefInterface(requireContext())
        mView = inflater.inflate(R.layout.fragment_mails_list, container, false)

        emailViewModel = ViewModelProvider(requireActivity()).get(EmailViewModel::class.java)

        btnSignOut = mView.findViewById(R.id.btn_sign_out)

        val pageTitle: TextView = mView.findViewById(R.id.tv_emails_fragment_title)

        val acct = GoogleSignIn.getLastSignedInAccount(activity)
        if (acct != null) {
            val personEmail = acct.email
            pageTitle.text =
                String.format(resources.getString(R.string.tv_emails_fragment_title), personEmail)
        }



        if (isOnline(requireActivity())) {
            getCredentials()
            service()
        }
        else{
            getAllEmailsFromDatabase()
        }

        GlobalScope.launch(Dispatchers.Main) {
            delay(5000)
            adapter = MailsListAdapter(mails!!, mailsAttachments, service, requireContext())
            val recyclerView: RecyclerView = mView.findViewById(R.id.recycler_view)
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            recyclerView.adapter = adapter
            mView.findViewById<ProgressBar>(R.id.mail_list_loading)!!.visibility = View.GONE
        }

        return mView
    }

    private fun insertDataToDatabase() {
        for (mail in mails!!) {
            val currentMail = Email(0, mail["From"]!!, mail["Subject"]!!, mail["Date"]!!)
            emailViewModel.addEmail(currentMail)
        }
    }

    private fun getAllEmailsFromDatabase() {
        emailViewModel.readAllData.observe(requireActivity(), Observer { emails ->
            for (email in emails) {
                val currEmail = mutableMapOf<String, String>()
                currEmail["From"] = email.sender
                currEmail["Subject"] = email.subject
                currEmail["Date"] = email.date
                mails!!.add(currEmail)
            }
        })
        dialog(
            "No connection",
            "You are in offline mode, now you can't download attachments and see new messages",
            requireActivity(),
            false
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignOut?.setOnClickListener {
            signOut()
        }
    }

    private fun signOut() {
        mGoogleSignInClient.signOut()
            .addOnCompleteListener((context as Activity?)!!, OnCompleteListener<Void?> {
                Toast.makeText(context, "Signed out successfully", Toast.LENGTH_SHORT).show()
                sharedPref?.setPref(SharedPrefConsts.REMEMBER_USER, false)
                Navigation.findNavController(requireView())
                    .navigate(R.id.action_mailsList_to_fragmentMain)
            })
    }

    private fun getCredentials() {
        credential = GoogleAccountCredential.usingOAuth2(
            requireContext(), listOf(GmailScopes.GMAIL_READONLY)
        )
            .setBackOff(ExponentialBackOff())
            .setSelectedAccount(
                Account(
                    FirebaseAuth.getInstance().currentUser?.email,
                    BuildConfig.APPLICATION_ID
                )
            )
    }

    private fun service() {
        service = Gmail.Builder(
            NetHttpTransport(), AndroidJsonFactory.getDefaultInstance(), credential
        )
            .setApplicationName("Pichurchyk_P2")
            .build()

        GlobalScope.launch {
            readMessage()
        }
    }

    private suspend fun readMessage() {
        try {
            val executeResult: ListMessagesResponse? =
                withContext(Dispatchers.IO) {
                    service!!.users().messages()?.list("me")?.setQ("to:me")?.execute()
                }
            val message: List<Message>? = executeResult!!.messages

            for (i in message!!.indices) {
                val messageRead = withContext(Dispatchers.IO) {
                    service!!.users().messages()
                        ?.get(FirebaseAuth.getInstance().currentUser?.email, message[i].id)
                        ?.setFormat("full")?.execute()
                }
                val currEmail = mutableMapOf<String, String>()
                val parts = messageRead!!.payload.parts
                val headers = messageRead.payload.headers
                var attachments = false

                if (parts != null) {
                    for (part in parts) {
                        if (part.getValue("filename").toString().isNotEmpty()) {
                            attachments = true
                            mailsAttachments.add(part)
                        }
                        currEmail["Attachments"] = attachments.toString()
                    }
                    if (!attachments) {
                        mailsAttachments.add(null)
                    }
                }
                else {
                    mailsAttachments.add(null)
                }
                for (value in headers) {
                    when (value.name) {
                        "From" -> {
                            val senderName = value.value.substring(0, value.value.indexOf("<"))
                                .replace("\"", "")
                            currEmail["From"] = senderName
                        }
                        "Subject" -> {
                            if (value.value.isEmpty()) {
                                currEmail["Subject"] = "No subject"
                            } else {
                                currEmail["Subject"] = value.value
                            }
                        }
                        "Date" -> {
                            val date = value.value.substring(0, 25)
                            currEmail["Date"] = date
                        }
                        else -> {
                            false
                        }
                    }
                }
                mails?.add(currEmail)
            }
            GlobalScope.launch(Dispatchers.Main) {
                adapter.notifyDataSetChanged()
            }
            println("------------------------------${mails!!.size}")
            insertDataToDatabase()
        } catch (e: UserRecoverableAuthIOException) {
            startActivityForResult(e.intent, RQ_GMAIL_ACCESS);
        }
    }

    fun isOnline(context: Context?): Boolean {
        val manager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = manager.activeNetworkInfo

        if (networkInfo != null) {
            if (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE)
                return true
        }
        toast("No internet connection")
        return false
    }
}