package com.example.pichurchyk_p2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.l2_t1_p.sharedPref.SharedPrefConsts
import com.example.l2_t1_p.sharedPref.SharedPrefInterface
import com.example.pichurchyk_p2.R
import com.example.pichurchyk_p2.common.BaseFragment
import com.example.pichurchyk_p2.databinding.FragmentMainBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class FragmentMain : BaseFragment(R.layout.fragment_main) {

    private lateinit var binding: FragmentMainBinding

    private var googleSignInClient: GoogleSignInClient? = null
    private val RC_SIGN_IN = 123
    private var auth: FirebaseAuth? = null

    private var sharedPref: SharedPrefInterface? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(layoutInflater, container, false)

        sharedPref = SharedPrefInterface(requireContext())

        auth = Firebase.auth

        createRequest()

        binding.btnSignIn.setOnClickListener {
            signIn()
            if (binding.checkRememberMe.isChecked) {
                sharedPref?.setPref(SharedPrefConsts.REMEMBER_USER, true)
            }
            else {
                sharedPref?.setPref(SharedPrefConsts.REMEMBER_USER, false)
            }
        }

        return binding.root
    }

    private fun createRequest() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)
    }

    private fun signIn() {
        val signInIntent = googleSignInClient?.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                println(e)
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth?.signInWithCredential(credential)
            ?.addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    Navigation.findNavController(requireView()).navigate(R.id.action_fragmentMain_to_mailsList)
                } else {
                    println(task.exception)
                }
            }
    }
}