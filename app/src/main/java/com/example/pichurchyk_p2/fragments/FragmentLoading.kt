package com.example.pichurchyk_p2.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.navigation.Navigation
import com.example.l2_t1_p.sharedPref.SharedPrefConsts
import com.example.l2_t1_p.sharedPref.SharedPrefInterface
import com.example.pichurchyk_p2.R
import com.example.pichurchyk_p2.common.BaseFragment
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FragmentLoading : BaseFragment(R.layout.fragment_loading) {

    private lateinit var sharedPref: SharedPrefInterface

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sharedPref = SharedPrefInterface(requireContext())

        val view = inflater.inflate(R.layout.fragment_loading, container, false)

        val progressBar: ProgressBar = view.findViewById(R.id.loading)
        GlobalScope.launch {
            delay(5000)
            activity?.runOnUiThread {
                progressBar.visibility = View.GONE
                if (sharedPref.getPrefBool(SharedPrefConsts.REMEMBER_USER) == true) {
                    Navigation.findNavController(requireView()).navigate(R.id.action_fragmentLoading_to_mailsList)
                }
                else {
                    if (isOnline(context)) {
                        Navigation.findNavController(requireView()).navigate(R.id.action_fragmentLoading_to_fragmentMain)
                    }
                    else {
                        dialog(
                            "No connection",
                            "You don't have access to the app until you find network",
                            requireActivity(),
                            true
                        )
                    }
                }
            }
        }
        return view
    }

    fun isOnline(context: Context?): Boolean {
        val manager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = manager.activeNetworkInfo

        if (networkInfo != null) {
            if (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE)
                return true
        }
        toast("No internet connection")
        return false
    }
}